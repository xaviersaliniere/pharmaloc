package objects;

import android.os.Parcel;
import android.os.Parcelable;

public class Pharmacie implements Parcelable {

	private String nom;
	private String commune;
	private String adresse;
	private String telephone;
	private String fax;
	private double latitude;
	private double longitude;
	private double distance;

	public Pharmacie(String nom, String commune, double latitude, double longitude){
		
		this.nom = nom;
		this.commune = commune;
		this.adresse = "Inconnu";
		this.telephone = "Inconnu";
		this.fax = "Inconnu";
		this.latitude = latitude;
		this.longitude = longitude;
		
	}

	public static final Parcelable.Creator<Pharmacie> CREATOR = new Parcelable.Creator<Pharmacie>()
	{
	    @Override
	    public Pharmacie createFromParcel(Parcel source)
	    {
	        return new Pharmacie(source);
	    }
	 
	    @Override
	    public Pharmacie[] newArray(int size)
	    {
	    return new Pharmacie[size];
	    }
	};
	 
	public Pharmacie(Parcel in) {
	    nom = in.readString();
	    commune = in.readString();
	    adresse = in.readString();
	    telephone = in.readString();
	    fax = in.readString();
	    latitude = in.readDouble();
	    longitude = in.readDouble();
	    distance = in.readDouble();
	}
	
	@Override
	public int describeContents() {

		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(nom);
		dest.writeString(commune);
		dest.writeString(adresse);
		dest.writeString(telephone);
		dest.writeString(fax);
		dest.writeDouble(latitude);
		dest.writeDouble(longitude);
		dest.writeDouble(distance);
	}
	
	private String phoneNumberFormat(String toFormat){
		
		String formated = "0";
		
		for(int i = 0; i < toFormat.length(); i++){
			formated += toFormat.charAt(i);
			
			if(i%2 == 0 && i != toFormat.length()-1)
				formated += ".";
		}
		
		return formated;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCommune() {
		return commune;
	}

	public void setCommune(String commune) {
		this.commune = commune;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = phoneNumberFormat(telephone);
	}
	
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = phoneNumberFormat(fax);
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

}
