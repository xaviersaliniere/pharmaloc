package adaptaters;

import java.util.List;

import org.lebtssio.pharmaloc.R;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import objects.Pharmacie;

public class FluxAdapter extends ArrayAdapter<Pharmacie> {

	private Activity activity;
	private List<Pharmacie> items;
	private Pharmacie objBean;
	private int row;

	public FluxAdapter(Activity act, int resource, List<Pharmacie> arrayList) {
		super(act, resource, arrayList);
		this.activity = act;
		this.row = resource;
		this.items = arrayList;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(row, null);

			holder = new ViewHolder();
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		if ((items == null) || ((position + 1) > items.size()))
			return view;

		objBean = items.get(position);
	
		holder.nom = (TextView) view.findViewById(R.id.nom);
		holder.commune = (TextView) view.findViewById(R.id.commune);		

		if (holder.nom != null && null != objBean.getNom()
				&& objBean.getNom().trim().length() > 0) {
			holder.nom.setText(Html.fromHtml(objBean.getNom()));
		}
		
		if (holder.commune != null && null != objBean.getCommune()
				&& objBean.getCommune().trim().length() > 0) {
			holder.commune.setText(Html.fromHtml(objBean.getCommune() + " (" + 
				Math.round(objBean.getDistance() * 100.0)/100.0 + "km)"));
		}
				
		return view;
	}

	public class ViewHolder {
		public TextView nom, commune;
	}
}