package org.lebtssio.pharmaloc;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import gps.GPSTracker;

public class MainActivity extends Activity {
	String pharmaJson;
	Double perimetre;
	EditText editPerimetre;
	Button retryButton;
	Button searchButton;
	GPSTracker gpstracker;
	public static final String SERVER_URL = "http://5.135.224.23/projects/pharmaloc-api/getpharmajson.php";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		editPerimetre = (EditText) findViewById(R.id.editPerimetre);
		
		retryButton = (Button) findViewById(R.id.retryJson);
		retryButton.setEnabled(true);
		retryButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				RecuperationJSon();
			}
			
		});
		
		searchButton = (Button) findViewById(R.id.searchbutton);
		searchButton.setEnabled(false);
		searchButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				
				if(editPerimetre.getText().toString().matches(""))
					editPerimetre.setError("Veuillez fournir le périmètre de recherche!");
				else {
					gpstracker.getLocation();
					if (!gpstracker.canGetLocation()) {
					    gpstracker.showSettingsAlert();
					}
					else {
						perimetre = Double.parseDouble(editPerimetre.getText().toString());
						LancementGO();
					}
				}
			}
		});
		
		RecuperationJSon();
		gpstracker = new GPSTracker(this);
	}
	
	private void RecuperationJSon() {
		retryButton.setVisibility(View.INVISIBLE);
		searchButton.setText("Récupération du JSON...");
		RequestQueue queue = Volley.newRequestQueue(this);

		// Demande une reponse de type String a partir de l'URL et en mode GET
		StringRequest stringRequest = 
				new StringRequest(Request.Method.GET, SERVER_URL,
						new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						Log.i("La reponse : ", response);
						pharmaJson = response;
						searchButton.setText("Rechercher");
						searchButton.setEnabled(true);
					}
				}, 
						new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						searchButton.setText("Récupération impossible");
						retryButton.setVisibility(View.VISIBLE);
					}
				});
		// Add the request to the RequestQueue.
		queue.add(stringRequest);
	}	

	private void LancementGO() {
		Intent intentGo = new Intent(MainActivity.this, GoActivity.class);
		intentGo.putExtra("perimetre",perimetre);
		intentGo.putExtra("pharmaJson",pharmaJson);
		this.startActivityForResult(intentGo,10);
	}
}
