package org.lebtssio.pharmaloc;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import objects.Pharmacie;

public class PharmaDetailActivity extends Activity {
	
	static final LatLng MELUN = new LatLng(48.543433,2.655573);
	Pharmacie pharma;
	TextView pharmaName;
	TextView pharmaAdress;
	TextView telView;
	TextView faxView;
	private GoogleMap map;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.pharma_detail, menu);
	    return true; 
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pharma_detail);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		pharma = this.getIntent().getExtras().getParcelable("pharma");
		
		pharmaName = (TextView)findViewById(R.id.pharmaName);
		pharmaName.setText(pharma.getNom());
		
		pharmaAdress = (TextView)findViewById(R.id.pharmaAdress);
		pharmaAdress.setText(pharma.getAdresse());
		
		telView = (TextView)findViewById(R.id.telView);
		telView.setText(pharma.getTelephone());
		
		faxView = (TextView)findViewById(R.id.faxView);
		faxView.setText(pharma.getFax());
		
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		
		Marker pharmaMarker = map.addMarker(new MarkerOptions()
		        .position(new LatLng(pharma.getLatitude(), pharma.getLongitude()))
		        .title(pharma.getNom())
		        .snippet(pharma.getAdresse())
		        .icon(BitmapDescriptorFactory
		        .fromResource(R.drawable.pharma_marker)));
		
		pharmaMarker.showInfoWindow();
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(pharmaMarker.getPosition(), 18));
	}
}
