package org.lebtssio.pharmaloc;

import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import gps.GPSTracker;
import objects.Pharmacie;

public class MapActivity extends Activity {
	
	GPSTracker gpstracker;
	 ArrayList<Pharmacie> pharmas;
	 ArrayList<Marker> markers;
	 private GoogleMap map;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.map, menu);
	    return true;
	}
	
	@Override
   public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()) {
       case android.R.id.home:
           NavUtils.navigateUpFromSameTask(this);
           return true;
       default:
           return super.onOptionsItemSelected(item);
       }
   }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		gpstracker = new GPSTracker(this);
		
		pharmas = this.getIntent().getExtras().getParcelableArrayList("Pharmas");
		
		markers = new ArrayList<Marker>();
		
	    map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
	            .getMap();
	        Marker phone = map.addMarker(new MarkerOptions().position(new LatLng(gpstracker.getLatitude(), gpstracker.getLongitude())).title("Ma Posistion"));
	        
	        for(int i = 0; i < pharmas.size(); i++){	        
	        	map.addMarker(new MarkerOptions()
	            .position(new LatLng(pharmas.get(i).getLatitude(), pharmas.get(i).getLongitude()))
	            .title(pharmas.get(i).getNom())
	            .snippet(Math.round(pharmas.get(i).getDistance() * 100.0)/100.0 + " km")
	            .icon(BitmapDescriptorFactory
	                .fromResource(R.drawable.pharma_marker)));
	        }
	        
	        map.getUiSettings().setZoomControlsEnabled(true);

	        map.moveCamera(CameraUpdateFactory.newLatLngZoom(phone.getPosition(), 15));

	        map.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
	}
	
}
