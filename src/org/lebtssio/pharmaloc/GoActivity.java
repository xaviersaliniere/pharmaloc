package org.lebtssio.pharmaloc;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import adaptaters.FluxAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import gps.GPSTracker;
import maths.Distance;
import objects.Pharmacie;

public class GoActivity  extends Activity {
	double perimetre;
	String pharmaJson;
	JSONObject jsonResponse;
	JSONArray arrayJson;
	ArrayList<Pharmacie> items;
	ListView lv;
	TextView emptyMessage;
	GPSTracker gpstracker;
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.go, menu);
	    return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        case R.id.action_cart:
        	LancementMap();
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_go);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		gpstracker = new GPSTracker(this);

		perimetre = this.getIntent().getExtras().getDouble("perimetre");
		pharmaJson = this.getIntent().getExtras().getString("pharmaJson");
		
		items = new ArrayList<Pharmacie>();
		
		emptyMessage = (TextView)findViewById(R.id.empty);
		
		lv = (ListView)findViewById(R.id.list);
		lv.setClickable(true);
		lv.setEmptyView(emptyMessage);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				
				LancementDetail(position);
			}
			
		});
		

		try {
			jsonResponse = new JSONObject(pharmaJson);
			// Creation du tableau general a partir d'un JSONObject
			JSONArray jsonArray = jsonResponse.getJSONArray("pharmas");
			Pharmacie currentFlux = null;
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObj = jsonArray.getJSONObject(i);
				String numvoie = "", typvoie = "", voie = "";
				
				currentFlux = new Pharmacie(jsonObj.getString("rs"), jsonObj.getString("commune"), jsonObj.getDouble("lat"),
						jsonObj.getDouble("lng"));
			
				currentFlux.setDistance(Distance.getDistance(gpstracker.getLatitude(), gpstracker.getLongitude(),
						currentFlux.getLatitude(), currentFlux.getLongitude()));
				
				if(jsonObj.has("telephone")){
					currentFlux.setTelephone(jsonObj.getString("telephone"));
				}
				
				if(jsonObj.has("telecopie")){
					currentFlux.setFax(jsonObj.getString("telecopie"));
				}
				
				if(jsonObj.has("numvoie"))
					numvoie = jsonObj.getString("numvoie");
				
				if(jsonObj.has("typvoie"))
					typvoie = jsonObj.getString("typvoie");
				
				if(jsonObj.has("voie"))
					voie = jsonObj.getString("voie");
				
				currentFlux.setAdresse(numvoie + " " + typvoie + " " +
					voie + ", " + jsonObj.getString("cp") + " " + currentFlux.getCommune());
				
				if(perimetre == 0 || currentFlux.getDistance() < perimetre)
					items.add(currentFlux);
			}
			
			items = sortPharma(items);

			ArrayAdapter<Pharmacie> objAdapter = new FluxAdapter(this,R.layout.row, items);
			lv.setAdapter(objAdapter);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private ArrayList<Pharmacie> sortPharma(ArrayList<Pharmacie> items){
		ArrayList<Pharmacie> sortedItems = new ArrayList<Pharmacie>();
		int index = -1;
		
		// Tant que la liste pas ordonnée n'est pas vide
		while(items.size() > 0) {
			// Pour chaque pharmacie
			for(int i = 0; i < items.size(); i++) {
				
				// On compare avec la pharmacie sauvegardée et la pharmacie actuelle de la boucle for
				// Si index = -1, cela veut dire que c'est le premier tour, on sauvegarde la première pharmacie
				if(index == -1 || items.get(index).getDistance() > items.get(i).getDistance())
					index = i;
				
			}
			
			// On ajoute la pharmacie sauvegardée (la plus proche) à la liste ordonnée
			sortedItems.add(items.get(index));
			// On supprime la pharmacie sauvegardée de la liste pas ordonnée
			items.remove(index);
			// On remet l'index à -1 pour recommencer
			index = -1;
		}
		
		return sortedItems;
	}
	
	private void LancementDetail(int index){
		Intent intentGo = new Intent(GoActivity.this, PharmaDetailActivity.class);
		intentGo.putExtra("pharma",(Parcelable) items.get(index));
		this.startActivityForResult(intentGo,10);
	}
	
	private void LancementMap(){
		Intent intentGo = new Intent(GoActivity.this, MapActivity.class);
		intentGo.putParcelableArrayListExtra("Pharmas", (ArrayList<Pharmacie>) items);
		this.startActivityForResult(intentGo,10);
	}
	
}
