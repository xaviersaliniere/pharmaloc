package maths;

public class Distance {

	public static double getDistance(double DegLatA, double DegLonA, double DegLatB, double DegLonB){
		int rayonPlanete = 6378;
		
		double RadLatA = convertRad(DegLatA);
		double RadLonA = convertRad(DegLonA);
		double RadLatB = convertRad(DegLatB);
		double RadLonB = convertRad(DegLonB);
		
		return rayonPlanete * (Math.PI/2 - Math.asin(Math.sin(RadLatB) * Math.sin(RadLatA) + Math.cos(RadLonB - RadLonA)
		* Math.cos(RadLatB) * Math.cos(RadLatA)));
	}
	
	// Convert deg to rad
	private static double convertRad(double deg){
		return (Math.PI * deg)/180;
	}
}
